import cv
import time
from delta import compare
import Image
import sys
import copy

TIME_TH = .2
TICKRATE = 0.1

def frame_to_im(frame):
    return Image.fromstring('RGB', (frame.width, frame.height), frame.tostring())

def main(cam_index):
    cv.NamedWindow("w1")
    capture = cv.CaptureFromCAM(cam_index)
    old_im = None
    old_cmp = None
    last_time = time.time()
    a = 0
    while True:
        frame = cv.QueryFrame(capture)
        if old_im is None:
            #old_im = frame_to_im(frame)
            old_im = frame
            continue

        #cv.ShowImage('w1', frame)
        #im = frame_to_im(frame)
        im = frame

        #cmpare = compare(im, old_im, treshold=130)
        cmpare = cv.Norm(im, old_im)
        print('lol:', cmpare)
        if cmpare != old_cmp and time.time() - last_time > TIME_TH:
            last_time = time.time()
            sys.stdout.write('#' * ((a+1) * 10) + ' ' * (100) + '\r')
            sys.stdout.flush()
            a += 1
            a %= 3

        old_cmp = cmpare
        #old_im = im.copy()
        old_im = cv.CreateImage(cv.GetSize(im), 8, 3)
        cv.Copy(im, old_im)
        print 'hihihi'

        time.sleep(TICKRATE)

if __name__ == '__main__':
    import sys
    main(int(sys.argv[1]))

